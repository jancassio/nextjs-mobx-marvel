import { action, observable } from 'mobx'
import fetch from 'isomorphic-fetch'
import { marvel, getAuthParams, urlWithPath } from '../utils/marvel'
import { templateFromString } from '../utils/string'

class CharacterStore {
  @observable character = new Character()
}

class Character {
  @observable id = 0
  @observable name = ''
  @observable description = ''

  constructor (id, name, description = '') {
    this.id = id
    this.name = name
    this.description = description
  }
}

async function getCharacterById (id) {
    const path = marvel.api.endpoints.public.characters.base
    const subPath = templateFromString(marvel.api.endpoints.public.characters.id, "characterId")(id)

    const url = urlWithPath(path + subPath)
    const res = await fetch(url)
    const result = await res.json()
    const characterObj = result.data.results[0]

    const character = new Character(
      characterObj.id, 
      characterObj.name,
      characterObj.description
    )

    return { character }
}

export {
  CharacterStore,
  Character,
  getCharacterById
}