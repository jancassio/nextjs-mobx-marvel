import { action, observable } from 'mobx'
import fetch from 'isomorphic-fetch'
import { marvel, getAuthParams, urlWithPath } from '../utils/marvel'
import { templateFromString } from '../utils/string'

import { Character } from './character'

export class CharactersStore {
  @observable items = []
    
  @action addCharacter (character) {
    this.items.push(character)
  }

  @action addCharacters (characters) {
    this.items = this.items.concat(characters)
  }
}

const baseURL = marvel.api.endpoints.base + marvel.api.endpoints.public.base

export async function getCharacters() {
  const path = marvel.api.endpoints.public.characters.base

  const url = urlWithPath(path)
  const res = await fetch(url)
  const result = await res.json()

  result.data.results = result.data.results.map(char =>{
    return new Character(char.id, char.name)
  })

  return { result }
}