const express = require('express')
const next = require('next')
const { parse } = require('url')
const pathMatch = require('path-match')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const route = pathMatch()


app.prepare()
  .then(() => {
    const server = express()

    server.get('/about/:id', (req, res) => {
      const { pathname, query } = parse(req.url, true)
      
      if (!pathname) {
        return handle(req, res)
      }
      
      const characterId = pathname.split('/')[2]
      return app.render(req, res, '/about', {characterId})
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(3000, (err) => {
      if (err) throw err

      if (dev)
        console.log('> Server ready on http://localhost:3000')
    })
  })