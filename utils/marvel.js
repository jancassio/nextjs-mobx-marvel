import { md5 } from '../utils/crypto'

const marvel = require('../config.json').marvel

const baseURL = marvel.api.endpoints.base + marvel.api.endpoints.public.base
const publicKey  = marvel.api.keys.public
const secretKey  = marvel.api.keys.secret

const getAuthParams = () => {
  let ts = (new Date()).getTime()
  let hash = md5(ts + secretKey + publicKey)
  return `ts=${ts}&apikey=${publicKey}&hash=${hash}`
}

const urlWithPath = (path) => {
  return `${baseURL}${path}?${getAuthParams()}`
}

export {
  marvel,
  getAuthParams,
  urlWithPath
}