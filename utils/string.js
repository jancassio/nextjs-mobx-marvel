
/*
  Read template string fields inside a literal string
  and return a new function that can emulates
  template string behavior.

  @param params: A list of parameter names to be replaced by.
**/
function templateFromString (string, params) {
  return new Function (params, "return `" + string + "`;")
}

export {
  templateFromString
}
