export const md5 = (string) => {
  let crypto = require('crypto');
  return crypto.createHash('md5')
    .update(string)
    .digest("hex");
}