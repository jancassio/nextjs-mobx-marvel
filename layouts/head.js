import Head from'next/head'

export default ({title = 'Default Title'}) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
    </Head>
  )
}