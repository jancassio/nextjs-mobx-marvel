import Link from 'next/link'
import Head from './head'

export default ({ children, title = `Marvel Chars Explorer`}) => {
  return (
    <div>
      <Head title={title}/>
      <header>
        <h1>{title}</h1>
        <nav>
          <Link href="/"><a>Home</a></Link>
        </nav>
      </header>
      { children }
      <footer>
        Copyleft Jan Cássio
      </footer>
    </div>
  )
}

