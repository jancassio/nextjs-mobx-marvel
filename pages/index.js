require('es6-promise').polyfill()

import React from 'react'
import { observer, Provider } from 'mobx-react'
import DevTool from 'mobx-react-devtools';

import Layout from '../layouts/layout'
import Header from '../components/pageheader'
import Characters from '../components/characters'
import { CharactersStore, getCharacters } from '../stores'

@observer
export default class Index extends React.Component {
  
  constructor () {
    super()
    this.store = new CharactersStore()

    this.loadCharacters()
  }

  async loadCharacters () {
    const characters = await getCharacters()
    this.store.addCharacters(characters.result.data.results)
  }

  render () {
    return (
      <Provider store={this.store}>
        <Layout>
          <Characters />
          <DevTool/>
        </Layout>
      </Provider>
    )
  }
}
