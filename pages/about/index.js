import React from 'react'
import Router from 'next/router'
import { inject, observer, Provider } from 'mobx-react'

import Layout from '../../layouts/layout'
import Header from '../../components/pageheader'
import { CharacterStore, Character, getCharacterById } from '../../stores'

@observer
export default class About extends React.Component {

  constructor () {
    super()
    this.loadCharacter()
    this.store = new CharacterStore()
  }

  async loadCharacter () {
    const res = await getCharacterById(Router.query.characterId)
    this.store.character = res.character
  }

  characterDetails () {
    if (!this.store.character.id) {
      return <h3>Loading...</h3>
    }

    return <CharacterDetails />
  }

  render () {
    return (
      <Provider store={this.store}>
        <Layout>
          { this.characterDetails() }
        </Layout>
      </Provider>
    )
  }

}


@inject("store") @observer
class CharacterDetails extends React.Component {
  render () {
    return (
      <div>
        <h1>{this.props.store.character.name}</h1>
        <p>{this.props.store.character.description}</p>
      </div>
    )
  }
}
