import React from 'react'
import Link from 'next/link'
import { observer, inject } from 'mobx-react'

@inject("store") @observer
export default class Characters extends React.Component {
  render () {
    return (
      <div>
        <h1>List of characters</h1>
        <ul>
          { this.listOfCharacters(this.props.store.items) }
        </ul>
      </div>
    )
  }

  listOfCharacters (items) {
    if (items == undefined || !items.length) {
      return <h3>Loading...</h3>
    }
    
    return items.map( character => {
      return <Character 
        key={character.id} 
        id={character.id} 
        name={character.name} />
    })
  }
}


const Character = ({ id, name }) => 
<li>
  <Link href={`/about?characterId=${id}`} as={`/about/${id}`}>
    <a>{name}</a>
  </Link>
</li>
