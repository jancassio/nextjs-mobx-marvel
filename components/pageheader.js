import React from 'react'

export default ({imageURL, description}) =>
<div>
  <div>
    <image src={imageURL} alt={description} />
  </div>
</div>